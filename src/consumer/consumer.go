package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	nc, e := nats.Connect(nats.DefaultURL)
	if e != nil {
		log.Println("Fail to connect to brocker: ", e)
		os.Exit(1)
	}
	defer nc.Close()
	// Subscribe consumer for subject 1
	_, e = nc.Subscribe("sub1", func(m *nats.Msg) {
		fmt.Printf("Received C1: (%s) %s\n", m.Sub.Subject, string(m.Data))
		time.Sleep(1000 * time.Millisecond)
	})
	if e != nil {
		log.Println("Fail to subscribe sub1: ", e)
		os.Exit(1)
	}
	// Subscribe another consumer for subject 1
	_, e = nc.Subscribe("sub1", func(m *nats.Msg) {
		fmt.Printf("Received C2: (%s) %s\n", m.Sub.Subject, string(m.Data))
		time.Sleep(500 * time.Millisecond)
	})
	if e != nil {
		log.Println("Fail to subscribe sub1: ", e)
		os.Exit(1)
	}
	// Subscribe consumer for subject2
	_, e = nc.Subscribe("sub2", func(m *nats.Msg) {
		fmt.Printf("Received C3: (%s) %s\n", m.Sub.Subject, string(m.Data))
	})
	if e != nil {
		log.Println("Fail to subscribe sub2: ", e)
		os.Exit(1)
	}

	quitChannel := make(chan os.Signal, 1)
	signal.Notify(quitChannel, syscall.SIGINT, syscall.SIGTERM)
	<-quitChannel
}
