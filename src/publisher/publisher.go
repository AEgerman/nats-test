package main

import (
	"encoding/json"
	"github.com/nats-io/nats.go"
	"log"
	"os"
	"time"
)

func main() {
	nc, e := nats.Connect(nats.DefaultURL)
	if e != nil {
		log.Println("Fail to connect to brocker: ", e)
		os.Exit(1)
	}
	defer nc.Close()

	message := struct {
		Time string `json:"time"`
		Msg  string `json:"message"`
	}{
		Time: time.Now().Format(time.RFC3339Nano),
		Msg:  "Hello World",
	}
	for i := 0; i < 10; i++ {
		msgBype, e := json.Marshal(message)
		if e != nil {
			log.Println("Fail to marshall message: ", e)
			os.Exit(1)
		}

		e = nc.Publish("sub1", msgBype)
		if e != nil {
			log.Println("Fail to send message: ", e)
			os.Exit(1)
		}
		e = nc.Publish("sub2", msgBype)
		if e != nil {
			log.Println("Fail to send message: ", e)
			os.Exit(1)
		}
	}
}
