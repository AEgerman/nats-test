package brocker

import "github.com/nats-io/nats.go"

func Connect() (*nats.Conn, error) {
	connectUrl := "nats://127.0.0.1:4222"
	return nats.Connect(connectUrl)
}
