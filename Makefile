server.pull:
	docker pull nats

server.build:
	docker run -d --name nats-test -p 4222:4222 -p 6222:6222 -p 8222:8222 nats

server.start:
	docker start nats-test

server.stop:
	docker stop nats-test

server.destroy:
	docker stop nats-test
	docker rm nats-test