### NATS server

##### Pull image
```shell
make server.pull
```

##### Create container and run
```shell
make server.build
```

##### Start container
```shell
make server.start
```

##### Stop container
```shell
make server.stop
```

##### Destroy container
```shell
make server.destroy
```

### Tesing
1) Run **consumer.go**
<br>There are 3 consumer. 
<br>First and second are for sub1 to test than brocker sends messages for all consumers based on busy state. In processing there was set timeout to make fake busy situations.
<br>Third consumer is single for sub2
2) Run **publisher.go** to send messages in topics